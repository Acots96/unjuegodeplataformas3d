## PEC3


Este es un juego del género horror survival en tercera persona, compuesto por un único nivel que contiene un pequeño pueblo rodeado por montañas, y una carretera que sale del pueblo para dirigirse hacia el exterior.

El objetivo principal del juego consiste en escapar del pueblo, plagado de zombies, para llegar junto a los soldados que están esperando en la carretera a las afueras. Además, hay una zona secundaria escondida para que el jugador la encuentre. El juego hace uso de assets como Cinemachine para controlar las cámaras o ToonGasStation para el pueblo, entre otros. Personajes y animaciones se han extraído de la web de Mixamo.

Inicialmente el jugador poseerá 3 armas, dos de fuego para eliminar enemigos a distancia y una espada para los que estén cuerpo a cuerpo. Las teclas que puede usar son:
 - Moverse, saltar y girar: WASD (Mayus para correr), Espacio y ratón.
 - Disparar/Golpe de espada: botón izquierdo del ratón.
 - Apuntar: botón derecho del ratón.
 - Cambiar de arma (teclas numéricas): pistola=1, escopeta=2, espada=3.
 - Recargar: R (solo para armas de fuego).
 - Interactuar: E.
 - Acercar/alejar la cámara: Rueda del ratón.
 - El ratón desaparece y queda bloqueado: pulsar ESC para hacerlo visible y desbloquearlo. Para volver al juego basta con pulsar con el botón izquierdo del ratón sobre la pantalla del juego.
 
### Terreno:
El terreno está formado por un pequeño pueblo con edificios, carreteras y decoración. Dicho pueblo también está rodeado por montañas para limitar al jugador. Finalmente, el terreno consta de una carretera que sale del pueblo, dirigiéndose a las "afueras", donde se encuentran dos militares con una camioneta que esperan al jugador (zona final del juego) y la zona secundaria.

### Armas del jugador:
En total en el juego hay 3 tipos de armas:
 - Pistola: Arma de fuego a distancia principal. Posee cadencia y daño medios. Tiene cartuchos de 12 balas y el jugador empieza con un total de 36 balas.
 - Escopeta: Arma de fuego a distancia secundaria. Posee cadencia inferior a la pistola, pero es superior en daño. Tiene cartuchos de 5 balas y el jugador empieza con un total de 20. La cadencia de ambas armas viene determinada por el tiempo que dura la animación de disparo, y ambas funcionan mediante Raycast que comprueba el Collider de los zombies (la cabeza tiene otro Collider con un tag concreto, para provocar mucho más daño si el jugador les dispara a la cabeza, concretamente 2.5 veces el daño del arma).
 - Espada: Arma cuerpo a cuerpo. Arma con más daño, suficiente para matar a los enemigos débiles de un solo golpe. En lugar de utilizar un Raycast, la espada activa un collider solo durante una parte de la animación de atacar, y guarda una lista de los zombies golpeados durante ese ataque en particular, para evitar que se llame al OnTriggerEnter más de una vez por zombie en un mismo ataque.
 
### Animaciones del jugador:
 - Para las animaciones del jugador se ha utilizado el Mecanim de Unity y los layers del AnimatorController (con Mask) para poder mezclar animaciones de cuerpo inferior (idle, caminar, correr, girarse, saltar, morir) con animaciones exclusivas de cuerpo superior (disparar, recibir golpe, recargar, cambiar de arma):
   1. Base Layer (layer de cuerpo inferior): Compuesto de las animaciones básicas de idle, caminar, correr, morir, girarse y saltar (ambas dos últimas son BlendTree, teniendo tres y dos animaciones respectivamente, que dependen de la velocidad del jugador). Además de estas básicas, también contiene una serie de animaciones exclusivas para cuando el jugador apunta, con el objetivo de que se pueda desplazar lateralmente y hacia atrás (además de adelante) mirando siempre hacia delante sin tener que dejar de apuntar.
   2. UpBodyLayer (layer de cuerpo superior): A diferencia del layer anterior, este posee una Mask para que solo afecte a la parte superior del cuerpo, y está por debajo del otro layer, con lo que las animaciones de este layer sobreescriben a las del Base Layer, de este modo se consigue que pueda realizar acciones de cuerpo superior (apuntar y disparar, recargar, recibir golpe,...) sin tener que dejar de moverse. Consta de las animaciones correspondientes para cada arma: apuntar, disparar/golpear, correr, recibir golpe y recargar. Además, también tiene un sistema de pequeñas animaciones que sirven para simular el cambio de arma, independientemente de cual sea el arma en uso actual y la futura.
 - Solapamiento entre layers: Cuando el jugador se está moviendo, dependiendo de la velocidad, el UpBodyLayer ejecuta la animación de apuntar (idle y caminar en el Base Layer) o la de correr (también correr en el Base Layer). Cuando el personaje está corriendo solo se ejecuta la animación de correr en la parte superior, mientras que si el personaje está caminando o quieto, puede ejecutar las animaciones de disparar, cambio de arma, recargar y recibir golpe. La animación de correr puede ejecutarse en cualquier momento en ambos layers, ya que en ambos se accede al mismo tiempo desde el AnyState que tiene por defecto el AnimatorController. Para diferenciar el momento en que el jugador está apuntando, por lo tanto usando las animaciones de movimiento lateral sin correr, se utiliza un booleano que permite el acceso solo a esas animaciones del Base Layer y se restringe la velocidad máxima para no poder correr. Finalmente, para poder comprobar puntos concretos en el tiempo de las animaciones (porque las adquiridas no permitían utilizar AnimationEvents), se han utilizado AnimationBehaviours en los estados del AnimatorController para acceder a los momentos en que se inicia/acaba una animación, o a su método Update para controlar qué debe ocurrir en un instante de tiempo concreto.

### HUD:
El HUD proporciona al jugador la información necesaria: 
 - En la parte inferior están la vida, la cantidad de llaves que posee el jugador (centro), y la munición del arma que el jugador está usando en ese momento (derecha) que se muestra como "balasCargadas/balasRestantes" en caso de ser un arma de fuego. Todos estos números del HUD inferior tienen al lado un pequeño icono para dar información sobre qué es ese número: la vida tiene una cruz roja, el número de llaves tiene una llave cyan y la munición tiene un pequeño icono que indica el tipo de arma, el cual cambia cuando el jugador cambia de arma.
 - En la parte central está la mirilla, solo cuando el jugador esté apuntando formada por 4 rectángulos blancos (2 verticales y 2 horizontales) separados entre ellos para dejar un espacio en el centro.
 - En la parte inferior (ligeramente encima de los números e iconos) tiene una barra semitransparente con texto que da información al jugador sobre qué debe hacer al llegar a una nueva area (aparece durante un tiempo determinado), y un texto que aparece en la parte central con información de ayuda que dan algunos elementos como zonas inaccesibles sin llave.
 - En la parte superior derecha podemos encontrar el icono de un zombie junto a un número, que indica la cantidad de zombies que hemos matado.
 - Finalmente las pantallas de GameOver y Win contienen un panel negro y la información sobre qué hacer para seguir jugando.
 
### Zombies:
 - El jugador hallará zombies distribuídos por todo el terreno. Están diseñados para deambular (alternando entre movimiento aleatorio durante un tiempo aleatorio, y quietos durante un tiempo aleatorio también) y perseguir al jugador en el caso de que este se acerque lo suficiente como para que los zombies lo vean. Cuando el zombie está lo suficientemente cerca se detiene y ejecuta la animación de atacar. Si el jugador se aleja estos empiezan a moverse de nuevo (siempre caminando) hacia el personaje hasta que este supera una distancia máxima, en cuyo caso los zombies regresan a deambular. Para atacar al jugador, se ha utilizado un AnimationBehaviour en la animación de ataque que activa un CapsuleCollider en el brazo con el objetivo de que el jugador solo pueda recibir daño mientras dura dicha animación (concretamente mientras la animación esté entre el 25% y el 85% del tiempo), dado que si este Collider estuviese activado siempre, el jugador podría recibir daño siempre que lo tocase, incluso si el zombie no lo está atacando. Además, esto genera concordancia visual entre el golpe del zombie y el momento en el que el personaje recibe daño. Los zombies pueden dejar caer items (vida o munición) al morir (no siempre los mismos ni la misma cantidad).
 - Por otro lado, el jugador también encontrará otro tipo de zombies, aunque en menor cantidad, dispersados por el terreno, con diferente aspecto y comportamiento a los demás (están siempre idle), pero mucho más rápidos y con mejor vista, por lo que detectarán al jugador desde más lejos y empezarán a correr hacia él. Además, también son más fuertes y resistentes, por lo que el jugador necesitará más disparos para abatir a este tipo de zombie y esquivar los golpes a toda costa, ya que un solo golpe le quitará al jugador más del 70% de su vida o incluso lo matará. Sin embargo, buscarlos y acabar con ellos tiene su recompensa, ya que uno dejará caer una llave que dará acceso al jugador a una zona secreta.
 - Finalmente, además de los mencionados, el jugador hallará unos zombies muy particulares (misma apariencia que los especiales), pero con los que no podrá interactuar. Únicamente poseen un AnimatorController con unas animaciones concretas, y se encuentran en la zona secundaria/oculta.
 
### Efectos (sistemas de partículas):
 - Partículas con un material rojo que simulan la sangre. Se utilizan justo en el momento en el que el personaje o un zombie reciben un golpe. En el caso de los zombies, si reciben un disparo con la escopeta, el efecto es duplicado y de mayor tamaño, con el objetivo de simular la magnitud de la bala de una escopeta.
 - Partículas con un material grisáceo, para simular el golpe de un disparo sobre cualquier objeto sólido, es decir, que no sea un zombie.
 - Efecto de "fuego" al disparar, en el cañón del arma, prácticamente imperceptible por su tamaño.
 
### Items:
 - Repartidos por el escenario hay cajas pequeñas con una cruz blanca (vida) y cajas con el dibujo de unas balas (munición), normalmente unas muy cerca de otras. El item de vida restaura un 50% del total de los puntos de vida del personaje, mientras que la munición tiene un valor aleatorio de como mínimo el 70% del tamaño de un cartucho, para que no siempre sea la misma cantidad. Los zombies también pueden dejar caer estos 3 items, y uno de los zombies especiales dejará caer el item de la llave.
 
### GameOver y Win:
 - Tanto al morir el personaje como al llegar hasta el final, la pantalla hace un fade a negro y se muestra al jugador la cantidad de zombies matados y qué debe hacer para volver a empezar el nivel (pulsar cualquier tecla).
 
### Sonidos:
 - El juego está lleno de sonidos, la mayoría con un poco de aleatoriedad en el pitch para evitar que suenen siempre de la misma manera:
   - Recoger munición, vida y llaves, todos distintos entre ellos.
   - Recargar.
   - Disparo: ambas armas de fuego tienen un sonido distinto. La pistola tiene un disparo normal, mientras que la escopeta tiene un sonido disparo más fuerte seguido del clásico sonido de escopeta corredera.
   - Golpe de una bala: un sonido para para un zombie y otro para cualquier otra superficie.
   - Golpe de espada, con dos sonidos distintos también al golpear a un zombie (sordo, parecido al de la bala) y otro al golpear otra superficie (sonido metálico). Además, cuenta con el típico sonido de "cortar" el viento al golpear con una espada afilada como una katana o un mosquete, siempre que no se golpee algun objeto sólido o a un zombie, en cuyo caso se corta el sonido para dejar paso al sonido del golpe.
   - Cambiar de arma: para esto se han añadido dos sonidos, uno que se reproduce en el momento de guardar/sacar un arma de fuego y otro que se reproduce en el momento de guardar/sacar la espada. Dado que el sonido de guardar/sacar un arma de fuego es más largo que el movimiento en sí, se reproduce entero para guardar un arma de fuego y sacar otra arma de fuego también, y se reproduce solo una parte si el arma a guardar/sacar es la espada.
   - Caminar/correr: hay dos sonidos distintos para que simule el típico sonido doble de los pasos de una persona, siempre con pitch aleatorio y teniendo en cuenta si es el sonido de un pie o de otro (explicado en el script de StepAnimationBehaviour).
   - Saltar y aterrizar.
   - Arma lista, siempre tras haber sacado un arma de fuego (ambas tienen sonidos distintos).
   - Golpeado: hay 4 sonidos que puede reproducir el jugador al ser golpeado, de los cuales se elige uno aleatoriamente cada vez.
   - Morir.
   - Zombies: los zombies disponen de los mismos tipos de sonidos básicos que el jugador, con la diferencia de que tienen hasta 9 sonidos de gruñidos que ejecutan aleatoriamente con ligeras variaciones del pitch, para que no parezca que todos gruñen de la misma manera. Como el jugador, disponen de varios sonidos al ser golpeados, un sonido para morir y un sonido al golpear al jugador. Además, poseen tres gruñidos de ataque que se reproducen mientras estén atacando/persiguiendo al jugador.
   - Radio al inicio del juego, simulando que recibe un mensaje de radio de los soldados que están esperándolo fuera.
   - Canción "Thriller" de Michael Jackson en la zona secundaria/oculta.
   
### Apuntar:
 - El jugador hace uso de la FreeLookCamera del asset Cinemachine tanto para la vista normal como para apuntar. En el momento en el que el jugador apunta, se desactiva la cámara principal y se activa la cámara de apuntar, que también está colocada detrás del jugador (a menor distancia que la principal, justo detrás) y ligeramente desplazada hacia la derecha, para que el jugador pueda ver bien lo que se encuentra delante de él. A diferencia de la cámara principal, cuyo movimiento es libre respecto al jugador (en modo SimpleFollowWithWorldUp), esta cámara siempre está detrás del jugador en la misma posición, restringiendo su movimiento para que siempre mire hacia delante independientemente de hacia donde se mueva, pero rotando al girar la cámara por el movimiento del ratón (modo WorldSpace). Al activarse esta cámara secundaria, Cinemachine hace un blend entre ambas de 0.25 segundos, para que no sea un cambio de golpe y brusco, pero lo suficientemente rápido como para no hacer esperar al jugador. También se activa una mirilla para las armas de fuego, distinta para cada arma.
 
### Zona oculta:
 - El juego posee una zona oculta en el terreno, concretamente frente a la furgoneta con los soldados que están esperando al jugador. Dicha zona es un edificio con un Collider encargado de comprobar si el jugador ha conseguido la llave necesaria que deja uno de los cuatro zombies fuertes, en cuyo caso le permite interactuar y es automáticamente teletransportado (con un fade a negro previamente) hasta una plataforma que rodea el tejado. El jugador aparecerá encima de la parte frontal del edificio, y tendrá que rodearlo saltando de plataforma en plataforma hasta llegar a la parte trasera, donde se encontrará con una grata sorpresa. Como indicador externo, además de lo visual (encima del edificio), el jugador escuchará la canción "Thriller" de Michael Jackson, cuyo volumen irá aumentando a medida que se acerque.
 
### Survival incremental:
 - Para hacer el juego cada vez más difícil a medida que pasa el tiempo, hay distintos puntos distribuídos por el mapa que instancian zombies (todos los puntos al mismo tiempo) cada cierto intervalo de tiempo, siendo dicho intervalo cada vez menor hasta un mínimo, por lo que al final llega a un nivel de dificultad más o menos extrema en la que aparece un zombie por segundo en cada punto distribuído hasta un límite editable de zombies simultáneos por punto para no saturar el juego (actualmente son 50 por punto, habiendo 35 puntos, con un máximo total de 1750 zombies simultáneamente). La cantidad de zombies especiales es de 4 inicialmente pero, a diferencia de los zombies normales, estos se incrementan más lentamente, a razón de un zombie fuerte cada vez, en un punto elegido aleatoriamente.
 
### Añadidos personales:
 - Las armas han sido colocadas en la mano derecha del personaje (solo una activada al mismo tiempo) ya que las animaciones son diestras. Para simular un cambio de arma realista, también tiene colocadas por el cuerpo armas de adorno, que se activan cuando el jugador no las está usando. Por ejemplo, si el jugador está usando la pistola, están activadas la escopeta y la espada, pero si decide equiparse con la escopeta, entonces reproducirá la animación de guardar la pistola y sacar la escopeta, y en un punto medio de la animación se desactivarán tanto la pistola de la mano como la escopeta guardada, y se activarán la pistola guardada y la escopeta en la mano del jugador. Dichas armas "falsas" están en la cintura (pistola) y en la espalda (escopeta y espada).
 - Para obligar al personaje a apuntar hacia delante lo más estático posible, se ha utilizado el método LateUpdate, ya que permite modificar la orientación de un personaje con una animación al ser llamado después del método interno de Unity que actualiza las animaciones. Cuando el personaje no está corriendo, el LateUpdate corrige la orientación de su UpBody para que apunte hacia delante lo mejor que pueda, sobre todo cuando el jugador realiza la acción de apuntar, en cuyo caso el UpBody corrige su posición para apuntar en la dirección que apunte la cámara, incluído el movimiento vertical para apuntar hacia arriba o hacia abajo.
 - El disparo depende de la cámara que esté activa en ese momento. Cuando el jugador está jugando con la cámara principal, el disparo se realiza a la altura del cañón del arma en uso en ese momento, usando la dirección forward del transform principal, que apunta siempre hacia delante. Por otro lado, cuando el jugador está apuntando y, por lo tanto, está activa la cámara secundaria, al estar ligeramente desplazada hacia la derecha y con la mirilla en el centro de la pantalla, el disparo surge de la posición de la cámara y con su misma dirección en lugar del cuerpo y dirección frontal del personaje. El objetivo de este funcionamiento es que el jugador no tenga que pensar desde donde dispara, sino que al colocar la mirilla en el centro de la pantalla ya se le da a entender que el disparo irá hacia el centro de dicha mirilla.
 - Para evitar que el número de FPS del juego caiga a medida que aumenta el número de zombies, se ha hecho un pequeño algoritmo en GameManager (además del límite editable de zombies simultáneos por punto) que contiene una lista con todos los zombies sobre el terreno para comprobar la distancia entre el jugador y cada uno de ellos. Si la distancia entre un zombie y el jugador es superior a un límite concreto (80 actualmente), entonces dicho zombie es desactivado hasta que vuelve a estar lo suficientemente cerca del jugador. Esto hace que solo los zombies que estén dentro de ese límite sean visibles para el jugador, por lo que disminuye la cantidad de elementos a actualizar en el juego. Dado que es una acción pesada (recorrer una lista con cientos de GameObjects) y no necesaria constantemente, se utiliza un InvokeRepeating que ejecuta la acción 4 veces por segundo.
 - Ambas cámaras usan el asset de Cinemachine, concretamente el FreeLookCamera para poder rotar libremente. El personaje siempre se mueve en relación a la rotación de la cámara, por lo que al pulsar la tecla W siempre se moverá en dirección al forward de la cámara. Ambas cámaras tienen un pequeño offset horizontal (más notable en la de apuntar al estar más cerca del personaje) y también poseen el componente CinemachineCollider para que la cámara no traspase paredes u otros sólidos.
 
 
### Scripts:
 - GameManager: script principal que controla la información mostrada por pantalla, la muerte del jugador (junto con las reapariciones), el control de los zombies que ve el jugador, la transición del teletransporte a la zona oculta, reiniciar los zombies thriller y al terminar el juego.
 
 - Actor: script del que heredan los actores (PECController y ZombieController). Contiene lo básico que debe tener un actor en el juego, que consiste en la vida y el método TakeDamage (recibir daño). Además, contiene sonidos y otros métodos que pueden ser implementados en las subclases, pero no es necesario.
 
   - PECController: script que controla el comportamiento del personaje dirigido por el jugador. Se encarga de inidicar a GameManager la información que debe actualizar por pantalla, comprobar el input para llevar a cabo las acciones, actualizar su animator, moverlo mediante un Rigidbody, calcular la dirección en la que debe ir según la cámara y corregirla en el LateUpdate dependiendo de si está apuntando o no, gestionar los AnimationBehaviours de su AnimationController,... entre otras cosas.
   
   - ZombieController: script que gestiona el comportamiento (IA) de los zombies, su Collider del brazo con el que atacan al jugador, el cual funciona de una forma muy similar a la espada. También controla el funcionamiento de los zombies fuertes/especiales, ya que tienen el mismo funcionamiento que los zombies normales con la diferencia de que estos corren en lugar de caminar, y no se mueven al deambular, están solo en idle.
 
 - Weapon: script que contiene la información base del arma (tipo y cantidad) y contiene el método abstracto de atacar que debe implementar cualquier subclase que herede de esta.
 
   - FireWeaponController: script que implementan todas las armas de fuego (pistola y escopeta). Hereda de Weapon y gestiona la lógica de disparo (Raycast, daño y efectos) y recarga.
 
   - SwordController: script que gestiona la lógica de la espada. Hereda de Weapon e implementa el OnTriggerEnter para saber cuando ha golpeado a un zombie o a un objeto sólido. 
 
 - Item: script que contiene la información de un item que el jugador encuentre en el suelo o deje caer un zombie. Puede ser munición de ambas armas de fuego, vida o una llave.
 
 - NeedKeyZone: script con un Collider, una cantidad de llaves necesaria y un evento que se ejecuta cuando el jugador (previamente habiendo entrado al Collider) lo ejecuta al interactuar. Usado únicamente para acceder a la zona secundaria.
 
 
 - ZombieSpawner: script encargado de gestionar la aparición de zombies sobre el terreno. El script es componente de un GO que contiene una serie de GOs hijos distribuídos por el terreno, donde se instancia a los zombies. Así, el script hace una lista con todos los hijos e instancia los zombies ahí cada cierto tiempo, el cual va disminuyendo hasta llegar a un mínimo (de 10 a 1 segundos, con un incremento negativo de 0.05 segundos, por lo que tardará 3 minutos en llegar a instanciar un zombie por segundo). Contiene una lista con los zombies normales que debe spawnear aleatoriamente y el zombie de tipo fuerte/especial.
 
 - AnimationBehaviours: scripts añadidos en algunos estados de los AnimatorController del jugador y de los zombies para controlar casos concretos al inicio, final o durante los mismos:
 
   - ChangeWeaponAnimationBehaviour: script usado por el AnimatorController del jugador, concretamente en el UpBodyLayer, para indicar a PECController cuando el jugador ha empezado a cambiar de arma y cuando acaba. Posee un booleano para indicar si debe desactivar el arma actual o activar la siguiente, y otro para saber si ha finalizado todo el conjunto de animaciones para cambiar de arma.
   
   - FinishJumpAnimationBehaviour: script que utiliza su método Update para saber el tiempo normalizado del AnimatorStateInfo con el objetivo de aplicar fuerza de salto una única vez si se ha superado el 30% del tiempo e indicar que ha finalizado el salto tras el 70% del tiempo transcurrido.
   
   - ShootAnimationBehaviour: script que avisa a PECController del momento en el que se inicia y se acaba el disparo, con el objetivo de que PECController pueda generar el disparo en FireWeaponController al inicio, y de que pueda resetearlo al final para poder volver a disparar.
   
   - StepAnimationBehaviour: script que comprueba el tiempo de la animación e indica a PECController que debe reproducir el sonido de un paso. La frecuencia depende de si el jugador corre o no, y para simular unos pasos más realistas se va alternando entre un pie (35-40% del tiempo) y el otro (85-90% del tiempo) y se le indica a PECController.
   
   - SwordAttackAnimationBehaviour: script encargado de comprobar el tiempo de la animación de ataque e indicarle a la espada (SwordController) si debe activar o desactivar su Collider según el tiempo de la animación. También indica a SwordController el momento en el que debe reproducir el sonido de "cortar" el aire.
   
   - ZombieAttackAnimationBehaviour: script con el mismo comportamiento que SwordAttackAnimationBehaviour, pero indicándole al zombie que active o desactive el Collider de su brazo.
   
   - ThrillerZombieAnimationBehaviour: script que indica a GameManager que un zombie de la zona secundaria/oculta ha terminado y debe reiniciarse.
   
### Mejoras:
 - Mejorar el uso de layers y BlendTrees para perfeccionar las animaciones del personaje, puesto que actualmente no tiene el comportamiento 100% esperado en algunas situaciones particulares, como el movimiento del layer superior al correr tras saltar, entre otras.
 - Añadir AudioMixers para gestionar mejor los sonidos y mejorar los que ya hay (pasos del jugador).
 - Encontrar la forma de mejorar la performance del juego cuando hay muchos zombies, ya que ahora, si hay muchos cerca del jugador, se nota la caída de FPS.
 - Terreno: NavigationMesh sobre todo, que no se adapta bien al terreno y tanto los zombies como el personaje pueden pasar por zonas en las que no toquen el suelo.
 - Revisar el comportamiento de las cámaras para mejorar aspectos como la "persecución" del personaje según se mueva, o el componente CinemachineCollider que no acaba de funcionar bien, entre otros aspectos.

   
*(Ver la implementación del código y los comentarios para más detalles. Todos los elementos añadidos están en Assets/Pec/)*
 
 
Video link: https://youtu.be/DaJSYee8etU