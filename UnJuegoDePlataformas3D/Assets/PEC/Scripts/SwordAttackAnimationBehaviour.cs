﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordAttackAnimationBehaviour : StateMachineBehaviour
{

    private SwordController sword;
    private bool soundDone;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        sword = GameManager.Player.Sword;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        float t = stateInfo.normalizedTime;
        t -= (int)t;
        if (t < 0.9f) {
            sword.EnableCollider(t > 0.3f && t < 0.7f);
            if (!soundDone && t > 0.2f) {
                soundDone = true;
                sword.PlayAttackSound();
            }
        } else {
            soundDone = false;
            sword.ResetHits();
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        soundDone = false;
        sword.ResetHits();
        sword.EnableCollider(false);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
