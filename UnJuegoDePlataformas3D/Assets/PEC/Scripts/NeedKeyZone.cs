﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NeedKeyZone : MonoBehaviour {
    
    public int Amount;
    public bool IsLock;

    [SerializeField] private UnityEvent Event;


    public void LockIt(bool b) {
        IsLock = b;
    }


    public void InvokeEvent() {
        Event?.Invoke();
    }
}
