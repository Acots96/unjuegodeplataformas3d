﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieController : Actor {

    private enum State { Wander, Chase, Attack, GetHit, Dead }

    public bool IsStrong;
    public float DisapearDistance;

    [SerializeField] private float WalkSpeed, DetectionDistance, StopDistance, Damage, GetHitAnimationTime;
    [SerializeField] private Collider ArmCollider;
    [SerializeField] private GameObject MeshGO;

    [SerializeField] private List<GameObject> ItemsPrefabsToDrop;

    [SerializeField] private AudioSource[] GrowlSounds, AttackGrowlSounds;
    [SerializeField] private AudioSource AttackHitSound;

    private Transform tr;
    private Animator anim;
    private Rigidbody rb;
    private NavMeshAgent agent;

    private PECController player;

    private float walkAnimation, speed, targetSpeed, distance;
    private Vector3 targetDir;
    private bool alreadyHit, chasing, playerDead;

    private State state;


    private void Awake() {
        tr = transform;
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        agent = GetComponent<NavMeshAgent>();

        if (!IsStrong) {
            int r = Random.Range(0, 2);
            anim.SetFloat("WalkAnimation", r);
            WalkSpeed *= r == 0 ? 1.2f : 1f;
        }
        agent.speed = WalkSpeed;
        targetDir = tr.forward;
        state = State.Wander;
        ArmCollider.enabled = playerDead = false;
    }

    private void Start() {
        player = GameManager.Player;
        CheckPlayer();
        StartCoroutine(WanderEffect());
        Invoke("GrowlSoundInvoke", Random.Range(2f, 7f));
        Invoke("AttackGrowlSoundInvoke", Random.Range(2f, 7f));
    }


    private void Update() {
        if (state == State.Dead)
            return;

        // comprueba si ve al jugador
        CheckPlayer();
        
        speed = Mathf.Lerp(speed, targetSpeed, 0.1f);
        anim.SetFloat("Speed", speed / WalkSpeed);

        targetDir.y = 0;
        tr.forward = Vector3.Slerp(tr.forward, targetDir, 0.25f);

        if (state != State.Attack) {
            agent.destination = tr.position + (speed > 0f ? targetDir : Vector3.zero);
            rb.velocity = speed > 0 ? agent.velocity : Vector3.zero;
        }
    }



    private void CheckPlayer() {
        if (playerDead)
            return;
        if (player.IsDead) {
            playerDead = true;
            state = State.Wander;
            alreadyHit = false;
            anim.SetBool("IsAttack", false);
            return;
        }
        
        distance = Vector3.Distance(tr.position, player.transform.position);

        /*if (distance > DisapearDistance) {
            if (MeshGO.activeSelf)
                MeshGO.SetActive(false);
            return;
        }
        if (!MeshGO.activeSelf)
            MeshGO.SetActive(true);*/

        // wander
        if (distance > DetectionDistance && !chasing && state != State.GetHit) {  // player too far
            if (state == State.Wander)
                return;
            state = State.Wander;
            alreadyHit = false;
            return;
        } 

        // attack
        else if (distance <= StopDistance) {
            if (state == State.Attack)
                return;
            alreadyHit = false;
            state = State.Attack;
            agent.isStopped = true;
            targetSpeed = 0;
            anim.SetBool("IsAttack", true);
        }
        
        // chase
        else if (distance <= DetectionDistance) {
            if (state == State.Chase || state == State.Attack)
                return;
            Chase();
        }
    }

    private void Chase() {
        alreadyHit = false;
        agent.isStopped = false;
        state = State.Chase;
        chasing = true;
        targetSpeed = WalkSpeed;
        anim.SetBool("IsAttack", false);
        Invoke("CountDown", 10f);
    }
    // Para que persiga durante un minimo de 10 segundos, 
    // luego vuelve a wander si el jugador esta lejos
    private void CountDown() {
        chasing = false;
    }



    public void CheckArmCollider(float t) {
        ArmCollider.enabled = t > 0.25f && t < 0.85f && !alreadyHit;
        if (t > 0.9f) {
            alreadyHit = false;
            if (distance > StopDistance) {
                Chase();
            }
        }
    }



    /**
     * Bucle en el que hace el movimiento de wander
     * (se mueve un tiempo determinado, se para un tiempo determinado, repite)
     * y espera mientras acontecen los demás estados.
     * El fuerte solo espera.
     */
    private IEnumerator WanderEffect() {
        Transform playerTr = player.transform;
        while (state != State.Dead) {
            while (state == State.Attack) {
                targetDir = (playerTr.position - tr.position).normalized;
                yield return null;
            }
            while (state == State.Chase) {
                targetDir = (playerTr.position - tr.position).normalized;
                yield return null;
            }
            while (state == State.Wander) {
                if (!IsStrong) {
                    targetSpeed = WalkSpeed;
                    targetDir = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f));
                    Debug.DrawRay(tr.position, targetDir, Color.cyan);
                    float randTime = Random.value * 7.5f + Time.time + 2f;
                    agent.isStopped = false;
                    yield return new WaitWhile(() => randTime > Time.time && state == State.Wander);
                    if (state == State.Wander) {
                        targetSpeed = 0;
                        randTime = Random.value * 5f + Time.time + 2f;
                        agent.isStopped = true;
                        yield return new WaitWhile(() => randTime > Time.time && state == State.Wander);
                    }
                } else {
                    targetSpeed = 0;
                    agent.isStopped = true;
                    yield return new WaitWhile(() => state == State.Wander);
                }
            }
            yield return new WaitWhile(() => state == State.GetHit);
        }
    }



    public override void TakeDamage(float dmg) {
        if (state == State.Dead)
            return;
        if (!IsStrong) {
            state = State.GetHit;
            targetSpeed = 0;
            agent.isStopped = true;
        }
        Life = (int)Mathf.Max(0, Life - dmg);
        if (Life <= 0) {
            state = State.Dead;
            agent.updatePosition = false;
            agent.updateRotation = false;
            rb.isKinematic = true;
            GetComponent<Collider>().enabled = false;
            DieSound.pitch = Random.Range(0.7f, 1.3f);
            DieSound.Play();
            anim.SetTrigger("DoDie");
            DropItems();
            GameManager.UpdateZombiesKilled();
            Destroy(gameObject, 6f);
        } else {
            AudioSource getHit = HitSounds[Random.Range(0, HitSounds.Length)];
            getHit.pitch = Random.Range(0.7f, 1.3f);
            getHit.Play();
            if (!IsStrong) {
                Invoke("GetHitStop", GetHitAnimationTime);
                anim.SetTrigger("DoGetHit");
            }
        }
    }
    
    private void GetHitStop() {
        if (state == State.GetHit) {
            Chase();
        }
    }


    private void DropItems() {
        //drop the key if there is one
        foreach (GameObject item in ItemsPrefabsToDrop) {
            if (item.GetComponent<Item>().Type == Item.ItemType.Key) {
                ItemsPrefabsToDrop.Remove(item);
                Instantiate(item, transform.position, item.transform.rotation);
                break;
            }
        }
        int itemsNum = Random.Range(0, ItemsPrefabsToDrop.Count) + 1;
        for (int i = 0; i < itemsNum; i++) {
            GameObject item = ItemsPrefabsToDrop[Random.Range(0, ItemsPrefabsToDrop.Count)];
            ItemsPrefabsToDrop.Remove(item);
            Instantiate(item, transform.position, item.transform.rotation);
        }
    }


    /**
     * Cada cierto tiempo aleatorio se reproducen los gruñidos,
     * ya sean los de idle o los de atacar.
     */
    private void GrowlSoundInvoke() {
        AudioSource growl = GrowlSounds[Random.Range(0, GrowlSounds.Length)];
        if (state == State.Wander && gameObject.activeSelf) {
            growl.pitch = Random.Range(0.7f, 1.3f);
            growl.Play();
        }
        if (state != State.Dead)
            Invoke("GrowlSoundInvoke", growl.clip.length + Random.Range(5f, 10f));
    }

    private void AttackGrowlSoundInvoke() {
        AudioSource attack = AttackGrowlSounds[Random.Range(0, AttackGrowlSounds.Length)];
        if (state == State.Attack || state == State.Chase) {
            attack.pitch = Random.Range(0.7f, 1.3f);
            attack.Play();
        }
        if (state != State.Dead)
            Invoke("AttackGrowlSoundInvoke", attack.clip.length + Random.Range(3f, 6f));
    }



    public void AddKeyToDrop(GameObject keyPrefab) {
        ItemsPrefabsToDrop.Add(keyPrefab);
    }



    private int stepSoundIdx;

    // pitch distintos segun el paso que sea, para dar la sensacion
    // de alternar entre paso derecho e izquierdo
    public override void PlayStepSound(int i) {
        if (IsStrong) {
            AudioSource step = StepSounds[stepSoundIdx++ % StepSounds.Length];
            if (i == 0)
                step.pitch = Random.Range(0.7f, 1.1f);
            else
                step.pitch = Random.Range(0.9f, 1.3f);
            step.Play();
        }
    }



    private void OnTriggerEnter(Collider other) {
        PECController player = other.GetComponent<PECController>();
        if (player && !alreadyHit && !playerDead) {
            alreadyHit = true;
            AttackHitSound.pitch = Random.Range(0.7f, 1.3f);
            AttackHitSound.Play();
            player.TakeDamage(Damage * Random.Range(0.7f, 1.3f));
        }
    }

}
