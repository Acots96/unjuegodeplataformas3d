﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishJumpAnimationBehaviour : StateMachineBehaviour
{

    private bool forceAdded, finished;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        forceAdded = finished = false;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!forceAdded) {
            if (stateInfo.normalizedTime >= 0.3f) {
                forceAdded = true;
                GameManager.Player.JumpForce();
            }
        } else {
            if (stateInfo.normalizedTime >= 0.8f) {
                finished = true;
                GameManager.Player.FinishJump();
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!finished)
            GameManager.Player.FinishJump();
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
