﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public enum WeaponType { Pistol, Shotgun, Sword, None }

    [SerializeField] private Text LifeText, AmmoText, KeysText, HelpText, ZombiesKilledText, InfoText;
    [SerializeField] private GameObject PistolImage, ShotgunImage, SwordImage, 
        PistolTargetIcon, ShotgunTargetIcon, SwordTargetIcon;
    [SerializeField] private Image Panel, InfoPanel;

    [SerializeField] private string GameOverInfo, PressAnyKeyInfo, PressEInfo, LockInfo, StartInfo, WinInfo, KeyFoundInfo, FinishInfo;

    [SerializeField] private float FadeTime;

    private static GameManager Instance;

    [SerializeField] private PECController controller;
    public static PECController Player { get => Instance.controller; }

    [SerializeField] private Transform ThrillerZone;

    [SerializeField] private AudioSource WalkiTalkieSound;

    private int zombiesKilled;
    private bool finishInfoAlreadyShown;


    private void Awake() {
        if (Instance) {
            Destroy(Instance);
            Instance = null;
        }
        Instance = this;
        StartCoroutine(StartGameEffect());
        Player.CanMove = false;
        zombies = new List<GameObject>();
        InvokeRepeating("CheckZombiesPosition", 0.25f, 0.25f);
    }


    // actualiza la informacion del arma que tiene el jugador en ese momento
    public static void UpdateWeaponText(float loadedBullets, float cartridgesBullets) { // municion
        if (loadedBullets == -1)
            Instance.AmmoText.text = "";
        else
            Instance.AmmoText.text = loadedBullets + "/" + cartridgesBullets;
    }
    public static void UpdateWeaponImage(WeaponType type) { // icono
        bool pistol = type == WeaponType.Pistol,
            shotgun = type == WeaponType.Shotgun,
            sword = type == WeaponType.Sword,
            none = type == WeaponType.None;
        Instance.PistolImage.SetActive(pistol);
        Instance.ShotgunImage.SetActive(shotgun);
        Instance.SwordImage.SetActive(sword);
    }
    public static void UpdateWeaponTarget(WeaponType type) { // target (cuando apunta)
        bool pistol = type == WeaponType.Pistol,
            shotgun = type == WeaponType.Shotgun,
            sword = type == WeaponType.Sword,
            none = type == WeaponType.None;
        if (Instance.PistolTargetIcon)
            Instance.PistolTargetIcon.SetActive(pistol && !none);
        if (Instance.ShotgunTargetIcon)
            Instance.ShotgunTargetIcon.SetActive(shotgun && !none);
        if (Instance.SwordTargetIcon)
            Instance.SwordTargetIcon.SetActive(sword && !none);
    }


    public static void UpdateLife(int life) {
        Instance.LifeText.text = life + "";
    }

    public static void UpdateKeys(float keys) {
        Instance.KeysText.text = keys + "";
    }


    // fade de negro a transparente 
    // con sonido de walkie e info
    private IEnumerator StartGameEffect() {
        float progress = 1;
        Color c = Panel.color;
        c.a = progress;
        Panel.color = c;
        yield return new WaitForSeconds(FadeTime * 0.15f);
        while (progress > 0) {
            progress -= Time.deltaTime / (FadeTime * 0.15f);
            c = Panel.color;
            c.a = progress;
            Panel.color = c;
            yield return null;
        }
        Player.CanMove = true;
        if (WalkiTalkieSound)
            WalkiTalkieSound.Play();
        ShowGameInfo(StartInfo);
    }



    public static void Win() {
        Instance.StartCoroutine(Instance.GameOverEffect(Instance.WinInfo));
    }
    public static void GameOver() {
        Instance.StartCoroutine(Instance.GameOverEffect(Instance.GameOverInfo));
    }

    // corutina que hace el fade a negro, comunica al
    // jugador lo que debe hacer (pulsar una tecla)
    // y recarga la escena
    private IEnumerator GameOverEffect(string info) {
        HelpText.text = "";
        float progress = 0;
        while (progress <= 1) {
            progress += Time.deltaTime / FadeTime;
            Color c = Panel.color;
            c.a = progress;
            Panel.color = c;
            yield return null;
        }
        HelpText.text = info;
        HelpText.text += "\n\n\n";
        yield return new WaitForSeconds(FadeTime / 2f);
        HelpText.text += PressAnyKeyInfo;
        //
        yield return new WaitUntil(() => Input.anyKeyDown);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }



    public void TeleportToThrillerZone() {
        StartCoroutine(TeleportEffect());
    }

    private IEnumerator TeleportEffect() {
        float progress = 0;
        while (progress <= 1) {
            progress += Time.deltaTime / (FadeTime * 0.15f);
            Color c = Panel.color;
            c.a = progress;
            Panel.color = c;
            yield return null;
        }
        yield return new WaitForSeconds(FadeTime * 0.15f);
        Player.gameObject.SetActive(false);
        Player.transform.position = ThrillerZone.position;
        Player.gameObject.SetActive(true);
        yield return new WaitForSeconds(FadeTime * 0.15f);
        while (progress > 0) {
            progress -= Time.deltaTime / (FadeTime * 0.15f);
            Color c = Panel.color;
            c.a = progress;
            Panel.color = c;
            yield return null;
        }
    }

    public static void ThrillerZombieFinished(Transform tr) {
        tr.gameObject.SetActive(false);
        tr.Rotate(Vector3.up, 180);
        tr.gameObject.SetActive(true);
    }



    public static void ShowEmptyInfo() {
        Instance.HelpText.text = string.Empty;
    }

    public static void ShowPressEInfo() {
        Instance.HelpText.text = Instance.PressEInfo;
    }

    public static void ShowLockInfo() {
        Instance.HelpText.text = Instance.LockInfo;
    }



    public static void UpdateZombiesKilled() {
        Instance.zombiesKilled++;
        Instance.ZombiesKilledText.text = Instance.zombiesKilled + "";
    }



    public void ShowGameInfo(string info) {
        StartCoroutine(InfoTextEffect(info));
    }

    // muestra el texto con barra semitransparente que indica
    // al jugador el objetivo de la zona a la que ha llegado
    private IEnumerator InfoTextEffect(string t) {
        float progress = 0, fadeTime = 0.5f;
        InfoText.text = t;
        while (progress <= 1) {
            progress = progress += Time.deltaTime / fadeTime;
            Color c = InfoPanel.color;
            c.a = progress * 0.75f;
            InfoPanel.color = c;
            c = InfoText.color;
            c.a = progress;
            InfoText.color = c;
            yield return null;
        }
        yield return new WaitForSeconds(10f);
        while (progress >= 0) {
            progress = progress -= Time.deltaTime / fadeTime;
            Color c = InfoPanel.color;
            c.a = progress * 0.75f;
            InfoPanel.color = c;
            c = InfoText.color;
            c.a = progress;
            InfoText.color = c;
            yield return null;
        }
    }



    public static void EndZone(bool inside) {
        Instance.EndZoneInstance(inside);
    }

    private void EndZoneInstance(bool inside) {
        if (inside) {
            if (!finishInfoAlreadyShown) {
                finishInfoAlreadyShown = true;
                ShowGameInfo(FinishInfo);
            }
            ShowPressEInfo();
        } else {
            ShowEmptyInfo();
        }
    }



    public static void ShowKeyFoundInfo() {
        Instance.ShowGameInfo(Instance.KeyFoundInfo);
    }



    private List<GameObject> zombies;

    public static void AddZombie(GameObject z) {
        Instance.zombies.Add(z);
    }

    // metodo ejecutado 4 veces por segundo para activar/desactivar
    // los zombies que esten fuera de un rango respecto al jugador.
    // el objetivo es mejorar la performance del juego
    private void CheckZombiesPosition() {
        Vector3 playerPos = Player.transform.position;
        List<int> reallyDeadIdx = new List<int>();
        //
        int idx = 0;
        foreach (GameObject zombie in zombies) {
            if (!zombie) {
                reallyDeadIdx.Add(idx);
                continue;
            }
            float distance = Vector3.Distance(playerPos, zombie.transform.position);
            if (distance > zombie.GetComponent<ZombieController>().DisapearDistance) {
                if (zombie.activeSelf)
                    zombie.SetActive(false);
            } else if (!zombie.activeSelf) {
                zombie.SetActive(false);
            }
            idx++;
        }
        // los que hayan sido desactivados se quitan de la lista
        // al final, para no tener que recorrer la lista principal
        // dos veces
        foreach (int i in reallyDeadIdx)
            zombies.RemoveAt(i);
    }
}
