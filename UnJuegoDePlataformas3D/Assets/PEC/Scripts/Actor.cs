﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Actor : MonoBehaviour {

    [SerializeField] protected int Life;

    [SerializeField] protected AudioSource[] HitSounds, StepSounds;
    [SerializeField] protected AudioSource DieSound;

    public bool IsPlayer { get; protected set; }

    public abstract void TakeDamage(float dmg);
    public virtual void Attack(float amount = 0) { }
    public virtual void AddAmmo(float amount) { }

    public virtual void PlayStepSound(int i) { }

}
